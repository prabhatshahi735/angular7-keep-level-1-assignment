import { Component, OnInit } from '@angular/core';
import { Note } from './note';
import { NotesService } from './notes.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor(private api: NotesService) { }

  errMessage: string;
  note: Note = new Note();

  noteList: Array<Note>;

  ngOnInit(): void {

    this.dataFetch();
  }

  dataFetch() {
    this.api.getNotes().subscribe((res) => {
      if (res) {
        this.noteList = res;
      } else {
        this.errMessage = 'Not able to fetch the data';
      }
    }, error => {
      this.errMessage = 'Http failure response for http://localhost:3000/notes: 404 Not Found';
    });
  }


  addData() {
    if (this.note.text !== '' || this.note.title !== '') {
      this.errMessage = 'Title and Text both are required fields';
    }

    this.api.addNote(this.note).subscribe((res) => {
      if (res) {
        this.noteList.push(res);
        this.note = new Note();
        this.dataFetch();
      } else {

      }
    }, error => {
      this.errMessage = 'Http failure response for http://localhost:3000/notes: 404 Not Found';
    });
  }

}

